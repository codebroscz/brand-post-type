<?php
/**
 * Plugin Name: Brand Post Type
 * Plugin URI: https://codebros.cz
 * Version: 1.0.3
 * Author: Vojtech Jurasek <vojtech@codebros.cz>
 * Author uri: https://codebros.cz
 * Description: Add Brand custom post type
 * Text Domain: brandposttype
 */

if (!defined('ABSPATH')) {
	exit; // don't access directly
};

if (!is_blog_installed()) {
	return;
}

function create_brand_type()
{
	$labels = array(
		'name' => __('Brands', 'brandposttype'),
		'singular_name' => __('Brand', 'brandposttype'),
		'add_new' => __('Add New', 'brandposttype'),
		'add_new_item' => __('Add New Brand', 'brandposttype'),
		'edit_item' => __('Edit Brand', 'brandposttype'),
		'new_item' => __('New Brand', 'brandposttype'),
		'view_item' => __('View Brand', 'brandposttype'),
		'view_items' => __('View Brands', 'brandposttype'),
		'search_items' => __('Search Brands', 'brandposttype'),
		'not_found' => __('No brands found', 'brandposttype'),
		'not_found_in_trash' => __('No brands found in Trash', 'brandposttype'),
		'all_items' => __('All Brands', 'brandposttype'),
		'archives' => __('Brand Archives', 'brandposttype'),
		'attributes' => __('Brand Attributes', 'brandposttype'),
		'insert_into_item' => __('Insert into brand', 'brandposttype'),
		'uploaded_to_this_item' => __('Uploaded to this brand', 'brandposttype'),
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
		'taxonomies' => ['industry'],
	);

	register_post_type('brand', $args);
}
add_action('init', 'create_brand_type');

function create_industry_taxonomy()
{
	$labels = array(
		'name'                       => _x('Industries', 'taxonomy general name', 'giacomov'),
		'singular_name'              => _x('Industry', 'taxonomy singular name', 'giacomov'),
		'search_items'               => __('Search Industries', 'giacomov'),
		'popular_items'              => __('Popular Industries', 'giacomov'),
		'all_items'                  => __('All Industries', 'giacomov'),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __('Edit Industry', 'giacomov'),
		'update_item'                => __('Update Industry', 'giacomov'),
		'add_new_item'               => __('Add New Industry', 'giacomov'),
		'new_item_name'              => __('New Industry Name', 'giacomov'),
		'separate_items_with_commas' => __('Separate industries with commas', 'giacomov'),
		'add_or_remove_items'        => __('Add or remove industries', 'giacomov'),
		'choose_from_most_used'      => __('Choose from the most used industries', 'giacomov'),
		'not_found'                  => __('No industries found.', 'giacomov'),
		'menu_name'                  => __('Industries', 'giacomov'),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array('slug' => 'writer'),
	);

	register_taxonomy('industry', 'brand', $args);
}
add_action('init', 'create_industry_taxonomy');

/**
 * Return only images when WP Instagram Widget is in use
 */
add_filter('wpiw_images_only', '__return_true');

add_filter('wpiw_list_class', function ($classes) {
	return 'glide__slides';
});

add_filter('wpiw_img_class', function ($classes) {
	return 'lazy';
});

/**
 * Provide next link for brand detail page
 */

add_filter('get_previous_post_where', function () {
	global $post, $wpdb;
	return $wpdb->prepare("WHERE p.menu_order > %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
});

add_filter('get_next_post_where', function () {
	global $post, $wpdb;
	return $wpdb->prepare("WHERE p.menu_order < %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
});

add_filter('get_previous_post_sort', function () {
	return "ORDER BY p.menu_order DESC LIMIT 1";
});

add_filter('get_next_post_sort', function () {
	return "ORDER BY p.menu_order ASC LIMIT 1";
});

/**
 * Reqiure widget functionality
 */
require_once('widget.php');
